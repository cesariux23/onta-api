from django.contrib.gis.db import models


class BusRoute(models.Model):
    name = models.CharField(max_length=150)
    route = models.LineStringField()

    def __str__(self):
        return self.name


class BusStop(models.Model):
    name = models.CharField(max_length=150)
    point = models.PointField()

    def __str__(self):
        return self.name
