from django.conf import settings
from django.contrib.gis.utils import LayerMapping
from django.core.management.base import BaseCommand
import os

from aquita.models import BusStop

attr_mapping = {
    'name': 'id',
    'point': 'Point',
}


class Command(BaseCommand):
    help = 'Imports stops from a single geojson feature collection'

    def handle(self, *args, **options):
        file_path = os.path.join(settings.GEOJSON_IMPORT_PATH, 'stops.geojson')
        lm = LayerMapping(BusStop, file_path, attr_mapping,
                          transform=False)
        lm.save(strict=True, verbose=True)
