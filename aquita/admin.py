from django.contrib.gis import admin
from .models import BusRoute, BusStop

admin.site.register(BusRoute, admin.OSMGeoAdmin)
admin.site.register(BusStop, admin.OSMGeoAdmin)
