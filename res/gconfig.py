pidfile = '/run/gunicorn/onta.pid'
bind = ['unix:/run/gunicorn/onta.sock']
user = 'www-data'
group = 'www-data'
accesslog = '/var/log/gunicorn/onta.access.log'
errorlog = '/var/log/gunicorn/onta.error.log'
raw_env = [
    'DJANGO_SETTINGS_MODULE=settings_production',
]
capture_output = True
worker_class = 'eventlet'
workers = 3
